#!/usr/bin/env python
#  subunit: extensions to python unittest to get test results from subprocesses.
#  Copyright (C) 2008  Robert Collins <robertc@robertcollins.net>
#
#  Licensed under either the Apache License, Version 2.0 or the BSD 3-clause
#  license at the users choice. A copy of both licenses are available in the
#  project source as Apache-2.0 and BSD. You may not use this file except in
#  compliance with one of these two licences.
#  
#  Unless required by applicable law or agreed to in writing, software
#  distributed under these licenses is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
#  license you chose for the specific language governing permissions and
#  limitations under that license.
#

"""List tests in a subunit stream."""

from optparse import OptionParser
import sys
import unittest

from subunit import DiscardStream, ProtocolTestCase

class TestIdPrintingResult(unittest.TestResult):

    def __init__(self, stream, show_times=False):
        """Create a FilterResult object outputting to stream."""
        unittest.TestResult.__init__(self)
        self._stream = stream
        self.failed_tests = 0
        self.__time = 0
        self.show_times = show_times
        self._test = None
        self._test_duration = 0
        
    def addError(self, test, err):
        self.failed_tests += 1
        self._test = test

    def addFailure(self, test, err):
        self.failed_tests += 1
        self._test = test

    def addSuccess(self, test):
        self._test = test

    def reportTest(self, test, duration):
        if self.show_times:
            seconds = duration.seconds
            seconds += duration.days * 3600 * 24
            seconds += duration.microseconds / 1000000.0
            self._stream.write(test.id() + ' %0.3f\n' % seconds)
        else:
            self._stream.write(test.id() + '\n')

    def startTest(self, test):
        self._start_time = self._time()

    def stopTest(self, test):
        test_duration = self._time() - self._start_time
        self.reportTest(self._test, test_duration)

    def time(self, time):
        self.__time = time

    def _time(self):
        return self.__time

    def wasSuccessful(self):
        "Tells whether or not this result was a success"
        return self.failed_tests == 0


parser = OptionParser(description=__doc__)
parser.add_option("--times", action="store_true",
    help="list the time each test took (requires a timestamped stream)",
        default=False)
parser.add_option("--no-passthrough", action="store_true",
    help="Hide all non subunit input.", default=False, dest="no_passthrough")
(options, args) = parser.parse_args()
result = TestIdPrintingResult(sys.stdout, options.times)
if options.no_passthrough:
    passthrough_stream = DiscardStream()
else:
    passthrough_stream = None
test = ProtocolTestCase(sys.stdin, passthrough=passthrough_stream)
test.run(result)
if result.wasSuccessful():
    exit_code = 0
else:
    exit_code = 1
sys.exit(exit_code)
