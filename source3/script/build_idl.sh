#!/bin/sh

ARGS="--includedir=../librpc/idl --outputdir $PIDL_OUTPUTDIR --header --ndr-parser --samba3-ndr-server --samba3-ndr-client $PIDL_ARGS --"
IDL_FILES="$*"

oldpwd=`pwd`
cd ${srcdir}

[ -d $PIDL_OUTPUTDIR ] || mkdir -p $PIDL_OUTPUTDIR || exit 1

PIDL="$PIDL $ARGS"

##
## Find newer files rather than rebuild all of them
##

list=""
for f in ${IDL_FILES}; do
        b=`basename $f .idl`
	outfiles="cli_$b.c $b.h ndr_$b.h srv_$b.c"
	outfiles="$outfiles cli_$b.h ndr_$b.c srv_$b.h"

	for o in $outfiles; do
	    [ -f $PIDL_OUTPUTDIR/$o ] || {
		list="$list $f"
		break
	    }
	    test "`find $f -newer $PIDL_OUTPUTDIR/$o`" != "" && {
		list="$list $f"
		break
	    }
	done
done

##
## generate the ndr stubs
##

if [ "x$list" != x ]; then
	# echo "${PIDL} ${list}"
	$PIDL $list || exit 1
fi

cd ${oldpwd}

exit 0

